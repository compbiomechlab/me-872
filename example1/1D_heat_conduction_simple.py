from dolfin import *
import numpy
import matplotlib.pyplot as plt

# Create mesh and define function space
mesh = IntervalMesh(10000,0,1)

# outputs info about the mesh
V = FunctionSpace(mesh, 'Lagrange', 1)

# Define boundary conditions
u0 = Expression('0', degree=1)
uL = Expression('1', degree=1)

def u0_boundary(x, on_boundary):
    tol = 1e-14;
    return abs(x[0]) < tol
bcL = DirichletBC(V, u0, u0_boundary)

def uL_boundary(x, on_boundary):
    tol = 1e-14;
    return abs(x[0]) > 1 - tol
bcR = DirichletBC(V, uL, uL_boundary)

f = Expression("pow(k,2)*cos(2*pi*k*x[0])", k=32, degree=1)
f.k = 16

# Define the weak form
u = TrialFunction(V)
v = TestFunction(V)
a = inner(nabla_grad(u), 0.05*nabla_grad(v))*dx
L = -f*v*dx

# represents u as a function in a finite element function space V
u = Function(V)

# solve linear system "a(u) = L" for u
solve(a == L, u, [bcL, bcR])

# plot solution and mesh
plot(u)
plt.show()


