from dolfin import *
import numpy
import matplotlib.pyplot as plt


class D_expression(Expression):
  def eval(self, value, x):
    if (x[0] < 0.1 and x[0] >= 0.0):
      value[0] = 2.5e-6
    elif (x[0] < 0.2 and x[0] >= 0.1):
      value[0] = 1.9e-6
    elif x[0] < 0.3 and x[0] >= 0.2:
      value[0] = 1.25e-6
    elif x[0] < 0.4 and x[0] >= 0.3:
      value[0] = 0.15e-6
    elif x[0] < 0.5 and x[0] >= 0.4:
      value[0] = 1.55e-6
    elif x[0] < 0.6 and x[0] >= 0.5:
      value[0] = 2.25e-6
    elif x[0] < 0.7 and x[0] >= 0.6:
      value[0] = 1.25e-6
    elif x[0] < 0.8 and x[0] >= 0.7:
      value[0] = 0.25e-6
    elif x[0] < 0.9 and x[0] >= 0.8:
      value[0] = 1.75e-6
    else:
      value[0] = 1.0e-6

class tau_expression(Expression):
  def eval(self, value, x):
    if (x[0] < 0.1 and x[0] >= 0.0):
      value[0] = 0.8e-3 
    elif (x[0] < 0.2 and x[0] >= 0.1):
      value[0] = 1e-3
    elif x[0] < 0.3 and x[0] >= 0.2:
      value[0] = 2.5e-3
    elif x[0] < 0.4 and x[0] >= 0.3:
      value[0] = 1.25e-3
    elif x[0] < 0.5 and x[0] >= 0.4:
      value[0] = 0.55e-3
    elif x[0] < 0.6 and x[0] >= 0.5:
      value[0] = 0.45e-3
    elif x[0] < 0.7 and x[0] >= 0.6:
      value[0] = 1.25e-3
    elif x[0] < 0.8 and x[0] >= 0.7:
      value[0] = 0.95e-3
    elif x[0] < 0.9 and x[0] >= 0.8:
      value[0] = 1.25e-3
    else:
      value[0] = 1.95e-3




# Create mesh and define function space
mesh = IntervalMesh(100,0,1)

# outputs info about the mesh
V = FunctionSpace(mesh, 'Lagrange', 1)

# Define boundary conditions
c0 = Expression('0.5', degree=1)
g = Expression('5e-6', degree=1)

def u0_boundary(x, on_boundary):
    tol = 1e-14;
    return abs(x[0]) < tol
bc = DirichletBC(V, c0, u0_boundary)

# Define the functional space
c = TrialFunction(V); v = TestFunction(V)

# Initial condition
c_1 = project(Expression('0.5', degree=1), V)

# Time step
Tend = 100*(pow(0.01,2))/(0.1*1.39e-05);
dt = Tend/1000;

# Define weak form
DG = FunctionSpace(mesh,"DG",0)
D = interpolate(D_expression(degree=1),DG)
tau = interpolate(tau_expression(degree=1),DG)
a = inner(D*nabla_grad(c), nabla_grad(v))*dx + c*v*(tau+1/dt)*dx
L = 1/dt*c_1*v*dx + g*v*ds

A = assemble(a)

# represents u as a function in a finite element function space V
c = Function(V)   # the unknown at a new time level
t = dt
#c1_interpolate = interpolate(c_1, V)

cnt = 0;

while t <= Tend:
    b = assemble(L)
    bc.apply(A, b)
    solve(A, c.vector(), b)

    print t
    t += dt
    c_1.assign(c)

    # plot solution and mesh
    plt.axis([0,1,0,1])
    plot(c)
    #plt.show()
    cnt = cnt + 1;
    #wiz = plot(c, range_min=0., range_max=1.) 
    #if(cnt%10 == 0):
    #    filename = "timedeptproblem"+str(cnt)+'.png'
    #	wiz.write_png(filename)


## Hold plot

