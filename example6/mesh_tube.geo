Geometry.HideCompounds = 0;

//For speeding up the mesh calculations
//Mesh.CharacteristicLengthExtendFromBoundary = 0;
//Mesh.CharacteristicLengthFromPoints=1;

Mesh.CharacteristicLengthFactor = 0.25;

Mesh.Algorithm    = 2; // (1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad) (Default=2)
Mesh.RecombineAll = 0;
Mesh.RemeshAlgorithm = 2; // (0=no split, 1=automatic, 2=automatic only with metis) (Default=0)
Mesh.RemeshParametrization = 7; // (0=harmonic_circle, 1=conformal_spectral, 2=rbf, 3=harmonic_plane, 4=convex_circle, 5=convex_plane, 6=harmonic square, 7=conformal_fe) (Default=4)

Mesh.Algorithm3D    = 4; // (1=Delaunay, 4=Frontal, 5=Frontal Delaunay, 6=Frontal Hex, 7=MMG3D, 9=R-tree) (Default=1)
Mesh.Recombine3DAll = 1;
Mesh.Optimize = 1;

Merge "Part1Test.STEP";

CreateTopology;
//#lc = 2;
//#Field[1] = Attractor;
//#Field[1].FacesList = {7,8,9,10};
//#Field[2] = Threshold;
//#Field[2].IField = 1;
//#Field[2].LcMin = lc / 3;
//#Field[2].LcMax = lc;
//#Field[2].DistMin = 0.15;
//#Field[2].DistMax = 0.5;
//#Background Field = 2;
//
Volume(1) = {1};

Physical Volume(1) = 1;
Physical Surface(1) = 1;
Physical Surface(2) = 2;
Physical Surface(3) = 3;
Physical Surface(4) = 4;
Physical Surface(5) = 5;
Physical Surface(6) = 6;



