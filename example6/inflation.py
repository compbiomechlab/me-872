from dolfin import *
import matplotlib.pyplot as plt

dolfin.parameters['form_compiler']['cpp_optimize_flags'] = '-O3'
dolfin.parameters["form_compiler"]["cpp_optimize"] = True
dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["quadrature_degree"] = 4
#dolfin.parameters['allow_extrapolation'] = True

mesh = Mesh("mesh_tube.xml")
n = FacetNormal ( mesh )
facetboundaries = MeshFunction('size_t', mesh, 'mesh_tube_facet_region.xml')
ds = Measure("ds")[facetboundaries]  
#File("facetboundaries.pvd") << facetboundaries

V = VectorFunctionSpace(mesh, "Lagrange", 2)
Q = FunctionSpace(mesh, "Lagrange",1)

Velem = VectorElement("Lagrange", mesh.ufl_cell(), 2) 
Qelem = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
W= FunctionSpace(mesh, MixedElement([Velem,Qelem]))

## Define variational problem
w = Function(W)
u,p = split(w);
v,q = TestFunctions(W)

d = u.geometric_dimension()
I = Identity(d)             # Identity tensor
F = I + grad(u)             # Deformation gradient
C = F.T*F                   # Right Cauchy-Green tensor
Ic = tr(C)
J = det(F)
press = Expression("p", p = 0.00, degree=1)
psi = (Ic - 3)*dx - p*(J - 1)*dx - dot(-press*n,u)*ds(1) - dot(-press*n,u)*ds(6)

F = derivative(psi, w, TestFunction(W))
Jac = derivative(F, w, TrialFunction(W))

bc1 = DirichletBC(W.sub(0), Expression(("0.0","0.0","0.0"), degree=2), facetboundaries,3)
bc2 = DirichletBC(W.sub(0), Expression(("0.0","0.0","0.0"), degree=2), facetboundaries,4)
bcs = [bc1, bc2]

problem = NonlinearVariationalProblem(F, w, bcs, Jac)
solver = NonlinearVariationalSolver(problem)

prm = solver.parameters
prm['newton_solver']['absolute_tolerance'] = 1E-7
prm['newton_solver']['relative_tolerance'] = 1E-6
prm['newton_solver']['maximum_iterations'] = 10
prm['newton_solver']['relaxation_parameter'] = 1.0
prm['newton_solver']['linear_solver']='umfpack'

displacementfile = File("displacement.pvd")

for pstep in range(0, 4):
	press.p += 0.1
	solver.solve()
	print "Load step = ", pstep
	displacementfile << (w.sub(0),float(pstep))

plot(w.sub(0), mode = "displacement") 
plt.show()



