from dolfin import *
import numpy
import matplotlib.pyplot as plt

Theta = pi
a, b = 2, 3.0
nr = 100  # divisions in r direction
nt = 200  # divisions in theta direction
mesh = RectangleMesh(Point(a, 0), Point(b, 1), nr, nt, 'crossed')

# First make a denser mesh towards r=a
x = mesh.coordinates()[:,0]
y = mesh.coordinates()[:,1]

def cylinder(r, s):
    return [r*numpy.cos(Theta*s), r*numpy.sin(Theta*s)]

x_hat, y_hat = cylinder(x, y)
xy_hat_coor = numpy.array([x_hat, y_hat]).transpose()
mesh.coordinates()[:] = xy_hat_coor


# Define function space
V = FunctionSpace(mesh, 'Lagrange', 1)

# Define boundary
class Left(SubDomain):
    def inside(self, x, on_boundary):
	return (x[1] < DOLFIN_EPS and x[1] > -DOLFIN_EPS and x[0] > -3 - DOLFIN_EPS and x[0] < -2 + DOLFIN_EPS)

class Right(SubDomain):
    def inside(self, x, on_boundary):
	return (x[1] < DOLFIN_EPS and x[1] > -DOLFIN_EPS and x[0] > 2 - DOLFIN_EPS  and x[0] < 3 + DOLFIN_EPS)

class Bottom(SubDomain):
    def inside(self, x, on_boundary):
	return sqrt(pow(x[1],2) + pow(x[0],2)) > 2 - DOLFIN_EPS and sqrt(pow(x[1],2) + pow(x[0],2)) < 2 + DOLFIN_EPS

class Top(SubDomain):
    def inside(self, x, on_boundary):
	return sqrt(pow(x[1],2) + pow(x[0],2)) > 3 - DOLFIN_EPS and sqrt(pow(x[1],2) + pow(x[0],2)) < 3 + DOLFIN_EPS

class Source(SubDomain):
    def inside(self, x, on_boundary):
        return True if (sqrt(pow(x[1]-2.5,2) + pow(x[0],2)) < 0.4 + DOLFIN_EPS ) else False

class Source2(SubDomain):
    def inside(self, x, on_boundary):
        return True if (abs(atan(x[1]/x[0])) >= 3*pi/8 - DOLFIN_EPS  and (sqrt(pow(x[1]-2.5,2) + pow(x[0],2)) > 0.4 - DOLFIN_EPS )) else False


# Initialize sub-domain instances
left = Left()
top = Top()
right = Right()
bottom = Bottom()
source = Source()
source2 = Source2()

# Initialize mesh function for interior domains
domains = CellFunction("size_t", mesh)
domains.set_all(0)
source.mark(domains, 1)
source2.mark(domains, 2)

# Initialize mesh function for boundary domains
boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)
left.mark(boundaries, 1)
top.mark(boundaries, 2)
right.mark(boundaries, 3)
bottom.mark(boundaries, 4)

dx = Measure("dx")[domains]
ds = Measure("ds")[boundaries]

u0 = Expression('20', degree=1)
bc = DirichletBC(V, u0, boundaries, 1)

# Define Neumann B.C.
g = Expression('-20/(pi*x[0])', degree=1)

# Define the functional space
u = TrialFunction(V)
v = TestFunction(V)

a = inner(nabla_grad(u), nabla_grad(v))*dx(0) + inner(0.001*nabla_grad(u), nabla_grad(v))*dx(1)  + inner(nabla_grad(u), nabla_grad(v))*dx(2) 
L = -g*v*ds(3) + Expression('1',degree=1)*v*dx(2) + Expression('1',degree=1)*v*dx(1) + Expression('0',degree=1)*v*dx(0)

# The unknown function is a Function of V
u = Function(V) 

print list_lu_solver_methods()
# solve linear system "a(u) = L" for u
solve(a == L, u, [bc])

file = File("2Dheatconduction.pvd")
file << u

# plot solution and mesh
plot(u,scale=0.,elevate=0.)
plt.show()

