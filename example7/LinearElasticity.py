from dolfin import *
from dolfin.cpp.mesh import *
import matplotlib.pyplot as plt

basename = 'square_mesh_unstructured_with_crack'

meshfilename = basename + '_param.xml'
facetfilename = basename + '_param_facet_region.xml'
mesh = Mesh(meshfilename)
V = VectorFunctionSpace(mesh, "Lagrange", 1)

boundaries = MeshFunction("size_t", mesh, facetfilename)
File('boundaries.pvd') << boundaries
ds = Measure('ds')[boundaries]

bcl = DirichletBC(V.sub(0), Constant((0.0)), boundaries, 1)
bcs = [bcl]


v = TestFunction(V)
u = TrialFunction(V)

d = u.geometric_dimension()
I = Identity(d)          

eps = sym(grad(u))
sigma_far = 1.0

E, nu = 1.0, 0.3
sigma = E/(1 + nu)*(eps + nu/(1 - 2*nu)*tr(eps)*I)
a = inner(grad(v),sigma)*dx
t = Constant((sigma_far, 0))
L = inner(t, v)*ds(3)

u = Function(V)
solve(a == L, u, bcs)
fileout = File("displacement.pvd");
fileout << u;


Strain = project(sym(grad(u)), TensorFunctionSpace(mesh,'DG',0))
eps = sym(grad(u))
Stress = E/(1 + nu)*(eps + nu/(1 - 2*nu)*tr(eps)*I)
Stressproject = project(Stress, TensorFunctionSpace(mesh,'DG',0))
file = File("Stress.pvd");
file << Stressproject;

plot(Stress[1,1])
plt.show()

