from dolfin import *
import numpy
import matplotlib.pyplot as plt

class E(Expression):
  def eval(self, value, x):
    if (x[0] < 0.1 and x[0] >= 0.0):
      value[0] = 2.0
    elif (x[0] < 0.2 and x[0] >= 0.1):
      value[0] = 1.5
    elif x[0] < 0.3 and x[0] >= 0.2:
      value[0] = 0.75
    elif x[0] < 0.4 and x[0] >= 0.3:
      value[0] = 1.5
    elif x[0] < 0.5 and x[0] >= 0.4:
      value[0] = 3.75
    elif x[0] < 0.6 and x[0] >= 0.5:
      value[0] = 0.75
    elif x[0] < 0.7 and x[0] >= 0.6:
      value[0] = 1.25
    elif x[0] < 0.8 and x[0] >= 0.7:
      value[0] = 0.75
    elif x[0] < 0.9 and x[0] >= 0.8:
      value[0] = 2.00
    else:
      value[0] = 1.0


# Create mesh and define function space
mesh = IntervalMesh(1000,0,1)

# outputs info about the mesh
V = FunctionSpace(mesh, 'Lagrange', 1)

# Define boundary conditions
u0 = Expression('0.1', degree=1)
uL = Expression('0.2', degree=1)

def u0_boundary(x, on_boundary):
    tol = 1e-14;
    return abs(x[0]) < tol
bcL = DirichletBC(V, u0, u0_boundary)

def uL_boundary(x, on_boundary):
    tol = 1e-14;
    return abs(x[0]) > 1 - tol
bcR = DirichletBC(V, uL, uL_boundary)

f = Expression("pow(k,2)*cos(2*pi*k*x[0])", k=32, degree=1)

# Define the weak form
u = TrialFunction(V)
v = TestFunction(V)
D = FunctionSpace(mesh,"DG",0)
A = interpolate(E(degree=1),D)
a = inner(A*nabla_grad(u), nabla_grad(v))*dx
L = -f*v*dx

# represents u as a function in a finite element function space V
u = Function(V)

# solve linear system "a(u) = L" for u
solve(a == L, u, [bcL, bcR])

# plot solution and mesh
plot(u)
plt.show()
#print u.vector()[1]


