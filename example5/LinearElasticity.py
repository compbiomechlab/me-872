from dolfin import *
from dolfin.cpp.mesh import *
import matplotlib.pyplot as plt

# Read in mesh and define function space
mesh = Mesh('square_mesh_unstructured_with_hole_param.xml')
V = VectorFunctionSpace(mesh, "Lagrange", 1)
#plot(mesh, interactive=True)

# Read in boundaries
boundaries = MeshFunction("size_t", mesh, "square_mesh_unstructured_with_hole_param_facet_region.xml")
bcl = DirichletBC(V, Constant((0.0,0.0)), boundaries, 1)
bcr = DirichletBC(V, Constant((1.0,0.0)), boundaries, 3)
bcs = [bcl, bcr]
File('boundaries.pvd') << boundaries

# Defining the problem 
v = TestFunction(V)
u = TrialFunction(V)

d = u.geometric_dimension()
I = Identity(d)             # Identity tensor

eps = sym(grad(u))

E, nu = 1.0, 0.3
sigma = E/(1 + nu)*(eps + nu/(1 - 2*nu)*tr(eps)*I)
a = inner(grad(v),sigma)*dx
t = Constant((0, 0))
L = inner(t, v)*ds

# Solving the problem
u = Function(V)
solve(a == L, u, bcs)
file = File("displacement.pvd");
file << u;


#plot(u, mode = "displacement", interactive=True)

Strain = project(sym(grad(u)), TensorFunctionSpace(mesh,'DG',0))
eps = sym(grad(u))
Stress = project(E/(1 + nu)*(eps + nu/(1 - 2*nu)*tr(eps)*I), TensorFunctionSpace(mesh,'DG',0))
file = File("Stress.pvd");
file << Stress;
plot(Stress[0,0])
plt.show()
